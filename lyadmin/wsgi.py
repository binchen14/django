##!/home/bitnami/envs/sys3.5/bin/python
"""
WSGI config for lyadmin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

# added bc
import sys
sys.path.append("/home/bitnami/code/python/djcode/lyadmin")

#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'lyadmin.settings')
os.environ['DJANGO_SETTINGS_MODULE']= 'lyadmin.settings'

application = get_wsgi_application()
