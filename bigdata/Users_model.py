from django.db import models

# Create your models here.

class Users(models.Model):
	user_id   = models.AutoField(primary_key=True)
	acct_date = models.DateTimeField(auto_now_add=True) 
	email	  = models.EmailField(max_length=50)
	username  = models.CharField(max_length=100)
	#password  = models.CharField(max_length=255) 
	firstname = models.CharField(max_length=40)
	lastname  = models.CharField(max_length=40)
	gender    = models.CharField(max_length=1 )    
	birthyear = models.IntegerField()
	grade     = models.IntegerField()
	state     = models.CharField(max_length=30)
	city	  = models.CharField(max_length=50)
	school	  = models.CharField(max_length=50)
	class Meta:
		db_table = 'users'
		managed  = False
	
