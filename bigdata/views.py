from django.shortcuts import render
from django.utils.translation import gettext as _
# Create your views here.

from .models import CircG6T1C5Scores
from .models import CubG5T2C3Scores
from .models import CylG6T2C3Scores
from .models import DeciG5T1C1Scores
from .models import DecidivG5T1C3Scores
from .models import DirG6T1C2Scores
from .models import EqnG5T1C5Scores
from .models import FactorG5T2C2Scores
from .models import FracG5T2C4Scores
from .models import FracG5T2C6Scores
from .models import FracdG6T1C3Scores
from .models import FracmG6T1C1Scores
from .models import GonG5T1C6Scores
from .models import LinstatG5T2C7Scores
from .models import MotG5T2C5Scores
from .models import NegG6T2C1Scores
from .models import ObsG5T2C1Scores
from .models import PertG6T1C6Scores
from .models import PertG6T2C2Scores
from .models import PosG5T1C2Scores
from .models import PropG6T2C4Scores
from .models import RatG6T1C4Scores
from .models import SecG6T1C7Scores
from .models import StatG5T1C4Scores
from .models import TreeG5T1C7Scores
from .models import History
from .models import Users

g5t1 = [
DeciG5T1C1Scores,
PosG5T1C2Scores,
DecidivG5T1C3Scores,
StatG5T1C4Scores,
EqnG5T1C5Scores,
GonG5T1C6Scores,
TreeG5T1C7Scores,
]

g5t2 = [
ObsG5T2C1Scores,
FactorG5T2C2Scores,
CubG5T2C3Scores,
FracG5T2C4Scores,
MotG5T2C5Scores,
FracG5T2C6Scores,
LinstatG5T2C7Scores,
]

g6t1=[
FracmG6T1C1Scores,
DirG6T1C2Scores,
FracdG6T1C3Scores,
RatG6T1C4Scores,
CircG6T1C5Scores,
PertG6T1C6Scores,
SecG6T1C7Scores,
]

g6t2 = [
NegG6T2C1Scores,
PertG6T2C2Scores,
CylG6T2C3Scores,
PropG6T2C4Scores,
]

g5t1_tab_chn_names = [ _("小数乘法"),  _("位置"),        _("小数除法"),  _("可能性"), 
					   _("简易方程"),  _("多边形面积"), _("植树问题") ]
g5t2_tab_chn_names = [ _("观察物体"),  _("因数与倍数"),  _("长方体和正方体"), _("分数的意义和性质"), 
					   _("图形的运动"), _("分数的加法减法"), _("折线统计图")]
g6t1_tab_chn_names = [ _("分数乘法"),   _("位置与方向"), _("分数除法"),  _("比"), _("圆"),  
				       _("百分数"),     _("扇形统计图")]
g6t2_tab_chn_names = [_("负数"), _("百分数"), _("圆柱"),  _("比例")]

terms = [
dict(name="Grade 5 Spring 五年级上", chap_models=g5t1, chap_chn_names=g5t1_tab_chn_names),
dict(name="Grade 5 Fall 五年级下",   chap_models=g5t2, chap_chn_names=g5t2_tab_chn_names),
dict(name="Grade 6 Spring 六年级上", chap_models=g6t1, chap_chn_names=g6t1_tab_chn_names),
dict(name="Grade 6 Fall 六年级下",   chap_models=g6t2, chap_chn_names=g6t2_tab_chn_names),
]

def read_row(mod_class, user_id):
	try:
		obj = mod_class.objects.filter(user__user_id=user_id)
		arr = obj.values().first()
		if arr:
			arr.pop('id',      None)
			arr.pop('user_id', None)
			np   = len(arr)
			ndid = 0 
			ndmd = 0
			for key in arr:
				ndmd +=  arr[key]
				ndid +=  1 if arr[key] > 0 else 0
			return [np, ndid, np-ndid, round(100*ndid/np), ndmd]
		else:
			return [0,0,0,0,0]
	except:
		return [0,0,0,0,0]


def read_scores_of_user_with_id(user_id):

	bigdata = []
	try:
		user = Users.objects.get(pk=user_id)
	except:
		return bigdata	
	else:	# user exists
		for term in terms:
			term_name       = term['name']
			chap_models     = term['chap_models']
			chap_chn_names  = term['chap_chn_names']
			chap_scores     = []	# scores for all chap in the term
			for chap in chap_models:
				chap_name   = chap_chn_names[chap_models.index(chap)] 
				chap_score  = [chap_name] + read_row(chap, user_id)
				chap_scores.append(chap_score)	
			bigdata.append(
				dict(name=term_name,chap_scores=chap_scores, num_chaps=len(chap_models))
			)
		return bigdata


def index(request):
	user_list = Users.objects.all().order_by("acct_date")
	for user in user_list:
		user.city = user.city if user.city != "unknown" else "****"
	context   = {"user_list": user_list} 
	return render(request, "bigdata/index.html", context)


def detail(request, user_id):
	bigdata = read_scores_of_user_with_id(user_id)
	try:
		user = Users.objects.get(pk=user_id)
	except:
		usertag = ""
	else:
		user.city = user.city if user.city != "unknown" else ""
		usertag   = ", ".join([user.lastname+" "+user.firstname, user.state+" "+user.city, user.school])
	context = {"bigdata":bigdata, "usertag":usertag}
	return render(request, "bigdata/detail.html", context)
