# Generated by Django 2.1.3 on 2018-11-20 04:01

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publisher', '0021_auto_20181120_1154'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='subject',
            unique_together={('subject',)},
        ),
    ]
