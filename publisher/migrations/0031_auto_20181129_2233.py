# Generated by Django 2.1.3 on 2018-11-29 14:33

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publisher', '0030_remove_problem_label'),
    ]

    operations = [
        migrations.AlterField(
            model_name='problem',
            name='headline',
            field=models.CharField(help_text='(一行概括本题内容，至少 3 个字)', max_length=40, validators=[django.core.validators.MinLengthValidator(3)], verbose_name='摘要'),
        ),
    ]
