# Generated by Django 2.1.3 on 2018-11-16 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publisher', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='label',
            name='ptype',
        ),
        migrations.AddField(
            model_name='problem',
            name='ptype',
            field=models.CharField(choices=[('填空题', '填空题'), ('选择题', '选择题')], default='填空题', max_length=20),
        ),
    ]
