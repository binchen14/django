# Generated by Django 2.1.3 on 2018-11-29 20:37

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('publisher', '0038_auto_20181130_0434'),
    ]

    operations = [
        migrations.AlterField(
            model_name='problem',
            name='text',
            field=models.TextField(validators=[django.core.validators.MinLengthValidator(5), django.core.validators.MaxLengthValidator(1000)], verbose_name='正文'),
        ),
    ]
