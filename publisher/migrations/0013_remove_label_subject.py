# Generated by Django 2.1.3 on 2018-11-19 16:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('publisher', '0012_auto_20181120_0032'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='label',
            name='subject',
        ),
    ]
