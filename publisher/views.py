from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth import login, authenticate
from django.contrib.auth.decorators import login_required

# Create your views here.

from .models import Choice,Problem,MultipleChoiceProblem,FillInBlankProblem
from .forms  import NameForm,FillInBlankProblemModelForm,MultipleChoiceProblemModelForm
from .forms  import ChoiceModelFormSet
from .forms  import SignUpForm

from rest_framework.views    import APIView
from rest_framework.response import Response
from .serializers import ProblemSerializer 

def index(request):
	problems = Problem.objects.all().order_by("-date_created")[:20]
	headers  = [problem.index_header() for problem in problems]
	context  = {"headers":headers}
	return render(request, "publisher/index.html", context)

def detail(request, pk):
	try:
		prob = Problem.objects.get(pk=pk)
		if prob.prob_type == "填空":
			child     = FillInBlankProblem.objects.get(pk=pk)
			prob_type = "fillblank"
			choices   = None
		elif prob.prob_type == "选择":
			child     = MultipleChoiceProblem.objects.get(pk=pk)
			prob_type = "choice"
			choices   = Choice.objects.filter(problem=pk) 
		else:
			child	  = None
			prob_type = None
			choices   = None
	except:
		child     = None
		prob_type = None
		choices   = None
	context = {"child":child, "problem_type":prob_type, "choices":choices}
	return render(request, "publisher/detail.html", context)

def multiplechoice(request, prob_id):
	problem = get_object_or_404(MultipleChoiceProblem, pk=prob_id)
	user_choice_id = request.POST.get('mc-prob-id-'+str(prob_id), 0)
	if user_choice_id:
		choice  = get_object_or_404(Choice, pk=user_choice_id)
		choice.votes += 1
		choice.save()
		if choice.correct:
			problem.num_correct += 1
			problem.save()
			context = {'problem':problem, 'flag':True,  'user_choice_id':int(user_choice_id)}
		else:
			problem.num_wrong += 1
			problem.save()
			context = {'problem':problem, 'flag':False, 'user_choice_id':int(user_choice_id)}
		return render(request, "publisher/answer_mc.html", context)
	else:
		return HttpResponseRedirect(reverse("publisher:detail", args=(prob_id,)))

def fillblank(request, prob_id):
	problem   = get_object_or_404(FillInBlankProblem, pk=prob_id)	
	userinput = request.POST.get('fb-prob-id-'+str(prob_id), None)
	if userinput:
		if problem.answer == userinput:
			problem.num_correct += 1
			problem.save()
			context = {'problem':problem, 'flag':True}
		else:
			problem.num_wrong   += 1
			problem.save()
			context = {'problem':problem, 'flag':False, 'userinput':userinput}
		return render(request, "publisher/answer_fb.html", context)
	else:
		return HttpResponseRedirect(reverse("publisher:detail", args=(prob_id,)))

def statistics(request, prob_id):
	problem = get_object_or_404(Problem, pk=prob_id)
	if problem.prob_type == "选择":
		problem = get_object_or_404(MultipleChoiceProblem, pk=prob_id)
	else:
		problem = get_object_or_404(FillInBlankProblem, pk=prob_id)
	context = {'problem':problem}
	return render(request, "publisher/statistics.html", context)

def get_name(request):
	if request.method == "POST":
		form = NameForm(request.POST)
		if form.is_valid():
			return HttpResponse("hello %s" % form.cleaned_data['your_name'] )
	else:
		form = NameForm()
	context = {'form':form}
	return render(request, 'publisher/name.html', context)

@login_required(login_url="publisher:login")
def create_fb_problem(request):
	if request.method == "POST":
		form = FillInBlankProblemModelForm(request.POST)
		if form.is_valid():
			prob = form.save(commit=False)
			if request.user.is_authenticated:
				prob.author = request.user
			prob.save()
			form.save_m2m()
			return HttpResponseRedirect(reverse("publisher:detail", args=(prob.id,)))
	else:
		form = FillInBlankProblemModelForm()
	context = {"form":form}
	return render(request, "publisher/create_fb_problem.html", context)

def create_mc_problem0(request):
	if request.method == "POST":
		form = MultipleChoiceProblemModelForm(request.POST)
		if form.is_valid():
			prob = form.save()
			return HttpResponseRedirect(reverse("publisher:detail", args=(prob.id,)))
	else:
		form = MultipleChoiceProblemModelForm()
	context = {'form':form}
	return render(request, "publisher/create_mc_problem.html", context) 


@login_required(login_url="publisher:login")
def create_mc_problem(request):
	if request.method == "POST":
		form = MultipleChoiceProblemModelForm(request.POST)
		if form.is_valid():
			prob = form.save(commit=False)
			if request.user.is_authenticated:
				prob.author = request.user
			prob.save()
			form.save_m2m()
			formset = ChoiceModelFormSet(request.POST, instance=prob)
			if formset.is_valid():
				formset.save()
				return HttpResponseRedirect(reverse("publisher:detail", args=(prob.id,)))
			else:
				prob.delete()
		else:
			formset = ChoiceModelFormSet(request.POST)
	else:
		form = MultipleChoiceProblemModelForm(initial={'prob_type':Problem.TYPE.选择})
		prob = MultipleChoiceProblem()
		formset = ChoiceModelFormSet(instance=prob)
	context = {'form':form, 'formset':formset}
	return render(request, "publisher/create_mc_problem.html", context) 

def vote(request, prob_id):
	problem  = get_object_or_404(Problem, pk=prob_id)
	if request.method == "POST":
		likeQ = request.POST.get('vote', 0)
		if likeQ == 0:		#user submitted form, but did not really vote	
			return HttpResponseRedirect(reverse("publisher:statistics", args=(prob_id,)))
		elif likeQ == "pos":
			problem.num_likes += 1
		elif likeQ == "neg":
			problem.num_dislikes += 1
		problem.save()
		return HttpResponseRedirect(reverse("publisher:index"))
	else:
		return HttpResponseRedirect(reverse("publisher:statistics", args=(prob_id,)))

class ProbList(APIView):

	def get(self, request):
		problems   = Problem.objects.all().order_by('-date_created')[:20]
		serializer = ProblemSerializer(problems, many=True)
		return Response(serializer.data)

def signup(request):
	if request.method == "POST":
		form = SignUpForm(request.POST)
		if form.is_valid():
			form.save()
			username     = form.cleaned_data.get('username')
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=username, password=raw_password)			
			login(request, user)
			return redirect('publisher:index')	
	else:
		form = SignUpForm()	
	return render(request, "publisher/signup.html", {'form':form})

