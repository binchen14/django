from django.db import models
from django.core.validators import MaxValueValidator,  MinValueValidator
from django.core.validators import MaxLengthValidator, MinLengthValidator
from model_utils import Choices

# for translation
from django.utils.translation import gettext as _

# Create your models here.
from django.contrib.auth.models import User

class Subject(models.Model):
	SUBJECT = Choices(("语文",  _("chinese")),   ("数学",  _("math")), 
					  ("物理",  _("physics")),   ("化学",  _("chemistry")),
					  ("历史",  _("history")),   ("政治",  _("politics")), 
					  ("经济",  _("economics")), ("天文",  _("astronomy")), 
					  ("地理",  _("geology")),   ("英语",  _("English")),
					  ("自然",  _("nature")),    ("哲学",  _("philosophy")), 
					  ("体育",  _("sports")),    ("娱乐",  _("fun")), 
					  ("计算机", _("computer")), ("电影",  _("movie")), 
					  ("教育",  _("education")), ("电视剧", _("drama")),
					  ("跨学科", _("multi-descipline")))
	subject = models.CharField("subject", max_length=20, choices=SUBJECT,
				default=SUBJECT.跨学科, unique=True)  
	#detail  = models.CharField("subject detail", max_length=50, default="", null=True, blank=True)
	
	def __str__(self):
		return self.get_subject_display()			

	class Meta:
		pass	
	
class Label(models.Model):
	"""used to classify each problem published"""
	STAGE   = Choices("小学","初中","高中","大学","跨阶段")
	GRADE   = Choices("一年级","二年级","三年级","四年级","五年级","六年级","跨年级")
	TERM    = Choices("上学期","下学期", "暑假","寒假","跨学期")

	stage   = models.CharField("阶段", max_length=20, choices=STAGE,  default=STAGE.跨阶段)
	grade   = models.CharField("年级", max_length=20, choices=GRADE,  default=GRADE.跨年级)  
	term    = models.CharField("学期", max_length=20, choices=TERM,   default=TERM.跨学期)  
	
	def __str__(self):
		labels = [self.stage, self.grade, self.term]
		return ", ".join(labels)

	class Meta:
		unique_together = ('stage', 'grade','term')

class Problem(models.Model):
	TYPE		  = Choices(("填空","FB"), ("选择","MC"))
	LEVEL		  = Choices(("简单","1"),("中等","2"),
						    ("稍难","3"),("很难","4"),("奥林匹克","5"))
	subjects      = models.ManyToManyField(Subject, verbose_name="subjects", 
											help_text="(can select more than one)")	
	prob_type	  = models.CharField("type", max_length=20, choices=TYPE,   default=TYPE.填空)
	level         = models.CharField("level", max_length=20, choices=LEVEL,  default=LEVEL.简单)  
	#label		  = models.ForeignKey("Label", verbose_name="年级", 
	#						blank=True,null=True,
	#						on_delete=models.SET_DEFAULT, default=1) 
	author		  = models.ForeignKey(User, default=4, on_delete=models.SET_DEFAULT, 
									  blank=True, null=True)
	headline	  = models.CharField("headline", max_length=40, blank=False, null=False,
									 validators=[MinLengthValidator(3)], 
									 help_text="(at least 3 characters)")
	text		  = models.TextField("body", validators=[MinLengthValidator(5), 
									MaxLengthValidator(1000)]
									)
	hint		  = models.CharField("hint", max_length=100, blank=True, null=True)
	date_created  = models.DateTimeField(auto_now_add=True, blank=True, null=True)
	lecture		  = models.TextField("explanation", validators=[MaxLengthValidator(1000)],
									 blank=True, null=True)
	#num_figs_in_text    = models.IntegerField(default=0,
	#						validators=[MinValueValidator(0), MaxValueValidator(4)])	
	#num_figs_in_lecture = models.IntegerField(default=0,
	#						validators=[MinValueValidator(0), MaxValueValidator(4)])
	num_correct			= models.IntegerField(default=0, validators=[MinValueValidator(0)])
	num_wrong			= models.IntegerField(default=0, validators=[MinValueValidator(0)])
	num_likes			= models.IntegerField(default=0, validators=[MinValueValidator(0)])
	num_dislikes		= models.IntegerField(default=0, validators=[MinValueValidator(0)])

	def __str__(self):
		return	self.headline + " " + self.date_created.strftime('%Y-%m-%d %H:%M:%S')

	def subject_str(self):
		subs = [ str(subject) for subject in self.subjects.all() ]
		subs.sort()
		return ", ".join(subs)

	def index_header(self):
		return [self.id, self.headline,  self.get_prob_type_display(), self.subject_str(), 
				self.get_level_display(),self.date_created, self.num_likes, self.num_dislikes]

class FillInBlankProblem(Problem):
	ANSWERTYPE  = Choices(("整数","integer"),
					      ("小数","decimal"),
					      ("分数","fraction"),
					      ("汉字","Chinese"),
						  ("英语","English"),
					      ("其他","N/A"))	# type of answer
	answer_type = models.CharField(max_length=20, choices=ANSWERTYPE, 
								default=ANSWERTYPE.汉字,
								verbose_name="answer type",
								help_text="(what user should input)") 
	#prefix      = models.TextField(validators=[MaxLengthValidator(100)], 
	#								default="答案：", help_text="(前缀)")
	answer	    = models.CharField(max_length=20, blank=False, null=False, 
									verbose_name="answer",
									help_text="(correct answer here, do not include unit)")
	appendix    = models.CharField(max_length=50, default=".", 
									verbose_name = "epilogue",
									help_text="(provide 'unit', say, $, here, if there is one)")

	def __str__(self):
		txt =  self.text + self.answer + self.appendix
		return txt

	class Meta:
		verbose_name        = "Filling Blank Problem"
		verbose_name_plural = "Filling Blank Problems"

class MultipleChoiceProblem(Problem):
	#_num_choices = models.IntegerField(default=4,
    #                    validators=[MaxValueValidator(5), MinValueValidator(2)],
	#					db_column="num_choices")

	@property
	def num_choices(self):
		#return Choice.objects.filter(problem=self).count()
		return self.choice_set.count()

	@num_choices.setter
	def num_choices(self, value):
		pass

	class Meta:
		verbose_name        = "Multiple Choice Problem"
		verbose_name_plural = "Multiple Choice Problems"

class Choice(models.Model):
	problem     = models.ForeignKey("MultipleChoiceProblem", on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	correct     = models.BooleanField("Correct?", blank=False, null=False, default=False)
	votes       = models.PositiveIntegerField(default=0)

	def __str__(self):
		#mark =  "&#x2705;" if self.correct else "&#x274E;"
		mark =  "✅" if self.correct else  "❎"
		return self.choice_text + " " + mark + " 问题：" + self.problem.headline 

	class Meta:
		unique_together = ('problem','choice_text')

#https://stackoverflow.com/questions/6195478/max-image-size-on-file-upload
class Figure(models.Model):
	problem   = models.ForeignKey("Problem",  on_delete=models.CASCADE)
	order     = models.IntegerField(default=1,
                        validators=[MaxValueValidator(4), MinValueValidator(1)])
	OWNER     = Choices("问题","选项","解答")
	owner     = models.CharField(max_length=20, choices=OWNER, default=OWNER.问题)
	model_pic = models.ImageField(upload_to='images/')


class Profile(models.Model):
	user = models.OneToOneField(User, on_delete=models.CASCADE)
	image = models.ImageField(default="default.jpg", upload_to="publisher/%Y/%m/%d")

	def __str__(self):
		return '{} profile'.format(self.user.username)

