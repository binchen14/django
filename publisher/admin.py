from django.contrib import admin

# Register your models here.

from .models import Label,FillInBlankProblem,MultipleChoiceProblem,Choice,Figure,Subject
from .models import Profile

admin.site.register(Label)
admin.site.register(FillInBlankProblem)
admin.site.register(MultipleChoiceProblem)
admin.site.register(Choice)
admin.site.register(Figure)
admin.site.register(Subject)
admin.site.register(Profile)
