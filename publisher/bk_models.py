from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator
from django.core.validators import MaxLengthValidator, MinLengthValidator
from model_utils import Choices

# Create your models here.
from django.contrib.auth.models import User

"""
class Author(models.Model):
	firstname = models.CharField(max_length=25)
	lastname  = models.CharField(max_length=25)
	email     = models.EmailField()
	address   = models.CharField(max_length=255)
	postcode  = models.IntegerField()
	username  = models.CharField(max_length=40)
	password  = models.CharField(max_length=40)
	hashed	  = models.CharField(max_length=255)
	company   = models.CharField(max_length=50)
	JOB       = Choices("教师","学生","工人","农民","工程师","军人","其他")
	job       = models.CharField(max_length=20, choices=JOB)
	cellphone = models.IntegerField()
"""
	
class Label(models.Model):
	"""used to classify each problem published"""
	STAGE   = Choices("小学","初中","高中","大学","省略")
	GRADE   = Choices("一年级","二年级","三年级","四年级","五年级","六年级","省略")
	TERM    = Choices("上学期","下学期", "暑假","寒假","省略")
	SUBJECT = Choices("语文","数学","英语","物理","化学","历史","天文", "地理", "政治",
					  "自然", "经济", "哲学","体育","娱乐")
	LEVEL   = Choices("简单","中等","稍难","很难","奥林匹克","省略")
	#TYPE    = Choices("填空", "填两空","填三空","填四空", "是非", "三选一", "四选一", "五选一")
	TYPE    = Choices("填空题", "选择题")

	stage   = models.CharField(max_length=20, choices=STAGE,  default=STAGE.小学)
	grade   = models.CharField(max_length=20, choices=GRADE,  default=GRADE.五年级)  
	term    = models.CharField(max_length=20, choices=TERM,   default=TERM.省略)  
	subject = models.CharField(max_length=20, choices=SUBJECT,default=SUBJECT.数学)  
	level   = models.CharField(max_length=20, choices=LEVEL,  default=LEVEL.简单)  
	ptype   = models.CharField(max_length=20, choices=TYPE,   default=TYPE.填空题)
	
	def __str__(self):
		labels = []
		for lab in [self.subject, self.ptype, self.level, self.stage, self.grade, self.term]:
			if lab != "省略":
				labels.append(lab)
		return ", ".join(labels)

class Problem(models.Model):
	label		  = models.ForeignKey("Label",  on_delete=models.SET_DEFAULT, default=1) 
	author        = models.ForeignKey(User,     on_delete=models.SET_DEFAULT, default=1)
	outline		  = models.CharField(max_length=40,  default="摘要:", blank=True, null=False,
									 validators=[MinLengthValidator(5)])
	text		  = models.TextField(validators=[MinLengthValidator(5), MaxLengthValidator(1000)])
	hint		  = models.CharField(max_length=100, default="无",    blank=True, null=True)
	date_created  = models.DateTimeField(auto_now_add=True, blank=True)
	lecture		  = models.TextField(validators=[MaxLengthValidator(1000)],
									 default="本题解答：")
	num_figs_text    = models.IntegerField(default=0,
						validators=[MinValueValidator(0), MaxValueValidator(4)])	
	num_figs_lecture = models.IntegerField(default=0,
                        validators=[MinValueValidator(0), MaxValueValidator(4)])

	def __str__(self):
		return	self.outline + " " + self.date_created.strftime('%Y-%m-%d %H:%M:%S')

class EntryProblem(Problem):
	num_entries = models.IntegerField(default=1,
                        validators=[MaxValueValidator(4), MinValueValidator(1)])

class ChoiceProblem(Problem):
	num_choices = models.IntegerField(default=4,
                        validators=[MaxValueValidator(5), MinValueValidator(2)])
	score		= models.IntegerField(default=0,
                        validators=[MinValueValidator(0)])

class Choice(models.Model):
	problem     = models.ForeignKey("ChoiceProblem", on_delete=models.CASCADE)
	choice_text = models.CharField(max_length=200)
	correct     = models.BooleanField(default=False)
	votes       = models.IntegerField(default=0)

	def __str__(self):
		#mark =  "&#x2705;" if self.correct else "&#x274E;"
		mark =  "✅" if self.correct else  "❎"
		return self.text + " " + mark + " 问题：" + self.problem.outline 

class Entry(models.Model):
	problem  = models.ForeignKey("EntryProblem", on_delete=models.CASCADE)
	TYPE     = Choices("整数","小数","分数","无")	# type of answer
	LEVEL    = Choices("简单","中等","稍难","很难","无")
	level    = models.CharField(max_length=20, choices=LEVEL)
	order    = models.IntegerField(default=1,
                        validators=[MaxValueValidator(4), MinValueValidator(1)])
	score    = models.IntegerField(default=1,
                        validators=[MinValueValidator(0)])
	sub_text    = models.TextField(validators=[MaxLengthValidator(100)], 
							    default="子题目/子步骤内容于此：")
	answer	    = models.CharField(max_length=20, default="请在此处提供答案！")
	answer_type = models.CharField(max_length=20, choices=TYPE) 
	appendix    = models.CharField(max_length=50, default="。(如有单位，请补充于此)")
	sub_lecture = models.TextField(validators=[MaxLengthValidator(300)],
								   blank=True, null=True,
								   default="本步骤的详解: ")

	def __str__(self):
		txt =  self.sub_text + self.answer + self.appendix
		if self.score != 0:
			txt += "分值: " + str(self.score) + "分。"
		if self.sub_lecture:
			txt += self.sub_lecture
		return txt

#https://stackoverflow.com/questions/6195478/max-image-size-on-file-upload
class Figure(models.Model):
	problem   = models.ForeignKey("Problem",  on_delete=models.CASCADE)
	order     = models.IntegerField(default=1,
                        validators=[MaxValueValidator(4), MinValueValidator(1)])
	OWNER     = Choices("问题","选项","解答")
	owner     = models.CharField(max_length=20, choices=OWNER, default=OWNER.问题)
	model_pic = models.ImageField(upload_to='images/')



