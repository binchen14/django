from django.urls import path

from .views import index,detail,fillblank,multiplechoice,statistics
from .views import get_name, create_fb_problem, create_mc_problem, vote
from .views import ProbList, signup
from django.contrib.auth.views import LoginView,LogoutView

app_name="publisher"
urlpatterns = [
	path("",					index,				name="index"),
	path("<int:pk>",			detail,				name="detail"),
	path("<int:prob_id>/fb",	fillblank,			name="fillblank"),
	path("<int:prob_id>/mc",	multiplechoice,		name="multiplechoice"),
	path("<int:prob_id>/statistics", statistics,	name="statistics"),
	path("name",                get_name,			name="name"),
	path("fb_create",			create_fb_problem,  name="create_fb_problem"),
	path("mc_create",			create_mc_problem,  name="create_mc_problem"),
	path("vote/<int:prob_id>",	vote,				name="vote"),
	path("rest",			    ProbList.as_view(), name="rest"),
	path('signup',				signup,             name="signup"),
	path('login',				LoginView.as_view(template_name="publisher/login.html"),   name="login"),
	path('logout',				LogoutView.as_view(template_name="publisher/logout.html"), name="logout"),

]
