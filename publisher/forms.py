from django import forms
from .models import Choice, FillInBlankProblem, MultipleChoiceProblem, Problem
from django.forms import inlineformset_factory
from django.forms import BaseInlineFormSet
from django.core.exceptions import ValidationError

from django.contrib.auth.forms  import UserCreationForm
from django.contrib.auth.models import User

class NameForm(forms.Form):
	your_name = forms.CharField(label="Your name", max_length=50)

class FillInBlankProblemModelForm(forms.ModelForm):
	class Meta:
		model  = FillInBlankProblem
		widgets = {
					'text':		forms.Textarea(attrs={'rows':3, 'cols':60}),
					'lecture':	forms.Textarea(attrs={'rows':3, 'cols':60}),
					'headline':	forms.TextInput(attrs={'size':40}),
					'hint':		forms.TextInput(attrs={'size':40}),
					'answer':	forms.TextInput(attrs={'size':40}),
					'appendix': forms.TextInput(attrs={'size':40}),
					'subjects': forms.SelectMultiple(attrs={'size':8, 'style': 'min-width:60px;'}),
				  }
		fields  = ('subjects', 'level',    'headline', 'text', 
				   'answer',   'appendix', 'answer_type', 'hint',    'lecture')
		#exclude = ('prob_type', 'num_correct', 'num_wrong', 'num_likes', 'num_dislikes')

class MultipleChoiceProblemModelForm(forms.ModelForm):
	class Meta:
		model = MultipleChoiceProblem
		widgets = {
					'text':		forms.Textarea(attrs={'rows':3, 'cols':60}),
					'lecture':	forms.Textarea(attrs={'rows':3, 'cols':60}),
					'headline':	forms.TextInput(attrs={'size':40}),
					'hint':		forms.TextInput(attrs={'size':40}),
					'subjects': forms.SelectMultiple(attrs={'size':8, 'style': 'min-width:60px;'}),
					'prob_type': forms.HiddenInput()
				  }
		fields  = ('subjects', 'level',    'headline', 'text', 
				    'hint',    'lecture',  'prob_type')

class MCInlineFormSet(BaseInlineFormSet):
	def clean(self):
		super().clean()
		count  = 0
		nforms = 0
		for form in self.forms:
			for key in form.cleaned_data.keys():
				if "correct" in key:
					count += form.cleaned_data.get(key, 0)
				if "choice_text" in key:
					if form.cleaned_data[key]:
						nforms += 1
		if nforms < 2 or nforms > 5:
			raise ValidationError("Please provide minum 2, maximum 5 chocies!") 
		if count != 1:
			raise ValidationError("Please mark one and only one answer as correct!")
			

class ChoiceModelForm(forms.ModelForm):
	class Meta:
		model   = Choice
		fields  = ('correct', 'choice_text')			
		widgets = {'choice_text': forms.TextInput(attrs={'size':40})}

ChoiceModelFormSet=inlineformset_factory(MultipleChoiceProblem, Choice,
						form = ChoiceModelForm, 
						max_num=5, extra=5,
						formset = MCInlineFormSet,
						can_delete=False
					)


class SignUpForm(UserCreationForm):
	first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
	last_name  = forms.CharField(max_length=30, required=False, help_text='Optional.')
	email      = forms.EmailField(max_length=254, help_text='Required. Valid')
	
	class Meta:
		model  = User
		fields = ('first_name', 'last_name', 'email', 'username', 'password1','password2')



